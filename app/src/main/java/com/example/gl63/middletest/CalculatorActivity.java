package com.example.gl63.middletest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;

import org.w3c.dom.Text;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textView4, textView6;
    private Button buttonSub, buttonDiv;
    private float[] result;
    private float num1, num2, temp;
    private String finalResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        result = getIntent().getFloatArrayExtra("EXTRA_SESSION_ID");

        buttonDiv = (Button) findViewById(R.id.buttonDiv);
        buttonDiv.setOnClickListener(this);
        buttonSub = (Button) findViewById(R.id.buttonSub);
        buttonSub.setOnClickListener(this);

        textView4 = findViewById(R.id.textView4);
        textView4.setText(String.valueOf(result[0]));
        textView6 = findViewById(R.id.textView6);
        textView6.setText(String.valueOf(result[1]));
    }

    @Override
    public void onClick(View v){
        num1 = Float.parseFloat(textView4.getText().toString());
        num2 = Float.parseFloat(textView6.getText().toString());
        switch (v.getId()){
            case R.id.buttonSub:
                //Sub
                temp = num1-num2;
                finalResult = num1 + " - " + num2 + " = " + temp;
                break;
            case R.id.buttonDiv:
                temp = num1/num2;
                finalResult = num1 + " / " + num2 + " = " + temp;
                break;
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra("EXTRA_SESSION_ID", finalResult);
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}