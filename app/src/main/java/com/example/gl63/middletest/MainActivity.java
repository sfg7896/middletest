package com.example.gl63.middletest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button sendButton;
    private EditText editNum1, editNum2;
    private float[] result = new float[2];
    private ArrayList arrayList;
    private ArrayAdapter arrayAdapter;
    private ListView listResults;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = (Button) findViewById(R.id.buttonSend);
        sendButton.setOnClickListener(this);
        editNum1 = findViewById(R.id.editNum1);
        editNum2 = findViewById(R.id.editNum2);
        listResults = findViewById(R.id.listResults);
        arrayList = new ArrayList<String>();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listResults.setAdapter(arrayAdapter);
    }

    @Override
    public void onClick(View v){
        if(editNum1.getText().toString().equals("") || editNum2.getText().toString().equals("")){
            Toast.makeText(this, "Enter the numbers!",
                    Toast.LENGTH_LONG).show();
        } else {
            result[0] = Float.parseFloat(editNum1.getText().toString());
            result[1] = Float.parseFloat(editNum2.getText().toString());
            Intent i = new Intent(MainActivity.this, CalculatorActivity.class);
            i.putExtra("EXTRA_SESSION_ID", result);
            startActivityForResult(i, 1);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                String listItem = data.getStringExtra("EXTRA_SESSION_ID");
                arrayList.add(listItem);
                arrayAdapter.notifyDataSetChanged();
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult

}
